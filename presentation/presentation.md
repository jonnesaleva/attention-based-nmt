---
title: Attentional Mechanisms in Neural Machine Translation
author: Jonne Sälevä
date: 02/27/2020
---

# Introduction

::: incremental

- In this talk, we will go over the attention-based NMT models that shook the MT community in 2014.
- Major impovement over seq2seq models and encoder-decoder architecture of @cho2014learning
- Find slides on [Gitlab](https://gitlab.com/jonnesaleva/attention-based-nmt)

:::

# Background: NMT formalism

- Input: sequence of words (1-hot vectors) in one language
$$
\mathbf{x} = (\mathbf{x}_1, \dots , \mathbf{x}_{T_x})
$$
- Output: sequence of words in another language
$$
\mathbf{y} = (\mathbf{y}_1, \dots , \mathbf{y}_{T_y})
$$
- We wish to discriminatively model the outputs conditional on the inputs

$$
p(\mathbf{y} | \mathbf{x})
$$


# Background: Encoder-decoder

- Introduced by @cho2014learning
- We train the discriminative model by maximizing 

$$
\log p(\mathbf{y} | \mathbf{x}) = \sum_{t=1}^{T_y} p(y_t | y_1, \dots, y_{t-1} \mathbf{x})
$$

- This kind of parametrization where $y_t$ depends on all past outputs is amenable to RNNs

# Background: Encoder-decoder

- Transform input symbols $\mathbf{x_t}$ into hidden representations $h_t$ via some type of RNN:

$$
h_t = \text{RNN}(\mathbf{x}_t, h_{t-1})
$$

- This encoder can be any type of RNN; in @cho2014learning and @bahdanau2014neural, a bidirectional GRU is used. Others use, for instance, (bi-)LSTMs.

# Background: Encoder-decoder

- After obtaining hidden representations $\{h_t\}_{t=1}^{T_x}$, use these hidden representations to somehow create a **context** representation $c$:

$$
c = q(h_1, \dots, h_{T_x})
$$

- In @cho2014learning, $q$ takes the form of simply the **last hidden state**

$$
c = q(h_1, \dots h_{T_x}) = h_{T_x}
$$

# Background: Encoder-decoder

- Now use RNN-based decoder to create hidden states $s_t$:

$$
s_t = \text{RNN}(y_{t-1}, s_{t-1}, c)
$$

- Then use a nonlinear function $g$ that maps to a vocabulary-sized vector:

$$
p(\mathbf{y}|\mathbf{x}) = \prod_{t=1}^{T_y} g(y_{t-1}, s_t, c)
$$

# @bahdanau2014neural

- Main contribution: invents **attention mechanism** to enhance encoder-decoder framework of @cho2014learning
- Motivation: The model of @cho2014learning is bad at translating long sentences
- Performance particularly bad when sentence longer than training data

# Bahdanau: Non-attention enhancements

- Firstly, use a **bidirectional** Gated Recurrent Unit to create the encoder-side hidden states.
- Motivation for this: we need "annotations" for each position that incorporate information from both past and the future
- Forward encoding: $\overrightarrow{h}_t = GRU(\overrightarrow{h}_{t-1}, x_t)$
- Backward encoding: $\overleftarrow{h}_t = GRU(\overleftarrow{h}_{t-1}, x_t)$
- Concatenate to obtain hidden representation

$$
h_t = [\overrightarrow{h}_t; \overleftarrow{h_t}]^\top
$$

# Gated Recurrent Unit

![Gated Recurrent Unit](https://qph.fs.quoracdn.net/main-qimg-d4725497028c5a60241e1524c32f60de)

- In our case, $\widetilde{h}_t = \tanh (WEx_t + U[r_t \circ \overrightarrow{h}_t])$
- Note: $E$ is an embedding matrix, so $Ex_t$ just picks the word embedding for the token at time $t$.
- Note: computation is same for forward and backward directions

# Bahdanau: Attention mechanism

- Idea: Instead of SMT-style hard-alignment, create a probabilistic _soft_ alignment over all input tokens
- This represents an expectation over a latent variable, i.e. hard alignment.
- This "expected alignment" is what we will use to compute the attention context vector at time $t$, $c_t$

# Bahdanau: Attention mechanism

- To align an output token $y_t$, first compute an unnormalized potential $e_{tj}$ for each encoder hidden state $h_j$:

$$
e_{tj} = a(s_{t-1}, h_j) = v_a^\top\tanh(W_a s_{t-1} + U_a h_j)
$$

- Note: $s_{t-1}$ is, again, the last hidden state from the decoder RNN
- Then normalize these potentials to obtain a probability that $y_t$ is aligned to $x_j$:

$$
\alpha_{tj} = \frac{\exp{e_{tj}}}{\sum_k \exp{e_{tk}}}
$$

- Then derive the context vector by computing a weighted sum of the encoder hidden states:

$$
c_t = \sum_{j=1}^{T_x} \alpha_{tj}h_j
$$

# Bahdanau: Decoder

- As with the encoder we use a GRU to compute the hidden states in the  decoder:

$$
s_t = \text{GRU}(y_{t-1}, s_{t-1}, c_t)
$$

- To generate the outputs, we use a combination of "deep output" and a maxout layer
- Then,  we calculate the output probabilities  as

$$
\widetilde{t}_t = (U_o s_{t-1} + V_o Ey_{t-1} + C_o c_t) \in \mathbb{R}^{2l}
$$

$$
t_t = [\max {\widetilde{t}_{t2j-1}, \widetilde{t}_{2j}}]^\top_{j=1,\dots l} \in \mathbb{R}^l
$$

$$
p(y_t |s_t, y_{t-1}, c_t) \propto \exp(y_t^\top W_o t_t)
$$

- Actual decoding happens using **beam search**

# Bahdanau: Experiment & training details

- Train basic E-D models: RNNencdec30, RNNencdec50
- Also train attention-based models: RNNsearch30, RNNsearch50
- Use data from ACL WMT 2014, 850M words in total
  - No monolingual data used beyond parallel En-Fr corpora
  - No other preprocessing beyond tokenization
- 30K most frequent words in both languages, map rest to UNK
- 1000 hidden units, word embedding dim 620, maxout layer size 500, alignment hidden layer size 1000

# Bahdanau: Experimental results
![](/home/citizen/Documents/coding/mt_seminar_project2/img/bahdanau_results_table.png)

- Importantly: RNNsearch performs as well as Moses which uses tons of unlabeled data! This also happened **one year** after NMT was invented.

# Bahdanau: Longer sentence handling

![](/home/citizen/Documents/coding/mt_seminar_project2/img/bahdanau_blue_curve.png)

- Dominance of RNNsearch becomes especially apparent when trained on longer sentences and when testing on longer sentences
- Suggests the previous E-D framework has trouble encoding a long sentence into a fixed context length

# Bahdanau: Qualitative observations

![](/home/citizen/Documents/coding/mt_seminar_project2/img/bahdanau_alignment.png){size=75%}

# Luong: Extending Bahdanau attention

- @luong2015effective acknowledge @bahdanau2014neural's original contribution
- Emphasize the importance of _exploring different architectures_
- Things we can tweak
    - RNN architecture of encoder and decoder (GRU vs LSTM)
    - Global vs local attention

# Luong: Encoding & decoding
- Luong uses a Stacked LSTM, not a GRU, as the encoder and decoder
- In this paper, decoding happens we generate an *attentional hidden state* by concatenating the context vector and hidden state, and pushing it through a single layer feedforward network

$$
\widetilde{h}_t = \tanh(W_c[c_t ; h_t])
$$

- Using this attentional hidden state, we sample an output token according to

$$
p(y_t | y_{<t}, x) = \text{softmax}(W_s \widetilde{h_t})
$$

# Stacked LSTM

![](/home/citizen/Documents/coding/mt_seminar_project2/img/stacked_lstm.png)

- For details, see @graves2013speech

# Luong: Details of attention computation

- The authors use a handy notation for the attention computation
- To align $y_t$ with source vector $\overline{h}_s$, we compute

$$
a_t(s) = \frac{\text{score}(h_t, \overline{h}_s)}{\sum_{s'}\text{score}(h_t, \overline{h})} = \text{align}(h_t, \overline{h}_s)
$$

- We have many ways of computing $\text{score}(h_t, \overline{h}_s)$:
  - dot product attention 
    - $\text{score}(h_t, \overline{h}_s) = h_t^\top\overline{h}_s$
  - generalized dot product (quadratic form)
    - $\text{score}(h_t, \overline{h}_s) = h_t^\top W_a \overline{h}_s$
  - concat attention
    -  $\text{score}(h_t, \overline{h}_s) = W_a [h_t; \overline{h}_s]$

# Luong: Attention (cont'd)

- In addition to these "content based" attention mechanisms there is a "location based" attention:

$$
a_t = \text{softmax}(W_a h_t)
$$

- Regardless of attention mechanism, the computation of the context vector $c_t$ remains the same:

$$
c_t = \sum_{s} a_t(s)\overline{h}_s
$$

# Luong: Global attention

![](/home/citizen/Documents/coding/mt_seminar_project2/img/luong_global_attention.png)

# Luong: Comparison to Bahdanau

- Overall, Luong both *extends* and *simplifies* things relative to the original model
- Extension: Stacked LSTM instead of GRU
  - No bidirectional anything here!
- Simplification: simpler computation path
  - Bahdanau: $h_{t-1} \rightarrow a_t \rightarrow c_t \rightarrow h_t \rightarrow \text{deep output + maxout} \rightarrow y_t$
  - Luong: $h_t \rightarrow a_t \rightarrow a_t \rightarrow c_t \rightarrow \widetilde{h_t} \rightarrow -> y_t$
- Extension: Luong experiments with multiple global attention schemes
- Extension: Luong experiments with *local* attention (coming up next)

# Luong: Local attention

- Global attention has a problem: it has to spread probability mass over entire source sentence when computing the expectation
- We want to focus on a *context window* around some alignment position.
- Question: how do we find this alignment position?

# Luong: Local attention

- Two main approaches to find alignment position $p_t$ at time $t$:
- Either assume alignments are monotonic: $p_t = t$
- Or: *predict* the position using the current hidden state

$$
p_t = S \sigma(v_p^\top \tanh(W_ph_t))
$$

- Then, define a context window $[p_t - D, p_t + D]$
  - D tuned empirically

# Luong: Local attention

- With monotonic alignment position, simply compute the weighted average over the context window
- However, with the predicted local position, we want to favor alignments near the center of the window
- Thus, transform the alignment scoring function by weighting it using a Gaussian density

$$
a_t(s) = \text{align}(h_t, \overline{h}_s)\exp(-\frac{(s-p_t)^2}{2\sigma})
$$

- The standard deviation $\sigma = \frac{D}{2}$ so that 95% falls within window

# Luong: Local attention

![](/home/citizen/Documents/coding/mt_seminar_project2/img/luong_local_attention.png)

# Luong: Input feeding

- Notice that these alignment decisions are made independently of other alignment decisions.
- Suboptimal! Take into account what has already been translated (c.f. *coverage vector* in SMT)
- At decode time, we concatenate $\widetilde{h}_t$ with the input at time t+1
- Hopefully this gives the model information about previous alignments

# Luong: Input feeding

![](/home/citizen/Documents/coding/mt_seminar_project2/img/luong_input_feeding.png)

# Luong: Experiment & training details

- Experiment on German - English language pair (unlike Bahdanau who used French - English)
- 50K most frequent words used in both languages, map rest to UNK
- Train on WMT 14  (116M English words, 110M German words)
- Also test on WMT 15 in both directions
- 4-layer stacked LSTMs, 1000 hidden units, and 1000-dimensional embeddings

# Luong: EN-DE results (WMT 14)

![](/home/citizen/Documents/coding/mt_seminar_project2/img/luong_results_table.png)

- New SOTA!

# Luong: EN-DE results (WMT 15)

![](/home/citizen/Documents/coding/mt_seminar_project2/img/bahdanau_wmt15_en_de.png)

- New SOTA!

# Luong: DE-EN results (WMT 15)

![](/home/citizen/Documents/coding/mt_seminar_project2/img/luong_de_en_wmt15.png)

# Luong: Translating long sentences

![](/home/citizen/Documents/coding/mt_seminar_project2/img/luong_length_analysis.png)

# Luong: Attentional architecture comparison

![](/home/citizen/Documents/coding/mt_seminar_project2/img/luong_attention_arch_comparison.png)

- Local attention with generalized dot product is the best in terms of perplexity and BLEU

# Luong: Visual attention comparison

![](/home/citizen/Documents/coding/mt_seminar_project2/img/luong_attention_visualizations.png)

# Luong: Obtaining hard alignments

![](/home/citizen/Documents/coding/mt_seminar_project2/img/luong_hard_alignment_comparison.png)

- Select source word with highest alignment score as the hard alignment
- AER scores comparable to Berkeley aligner!
- Local seems to be better than global

# References #

