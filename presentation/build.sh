INPUT_FILE="presentation.md"
OUTPUT_FILE="attentional_presentation.pdf"
BIBLIOGRAPHY_FILE="_pres.bib"

pandoc -t beamer --filter pandoc-citeproc --bibliography $BIBLIOGRAPHY_FILE -o $OUTPUT_FILE $INPUT_FILE
zathura $OUTPUT_FILE
