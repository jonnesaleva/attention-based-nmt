# Attention-based NMT 

This repsitory contains an analysis of Bahdanau et al.(2015) and Luong et al. (2015).

See `./presentation/` for details. :)

## References

Bahdanau, D., Cho, K. and Bengio, Y., 2015, January. Neural machine translation by jointly learning to align and translate. In 3rd International Conference on Learning Representations, ICLR 2015.

Luong, M.T., Pham, H. and Manning, C.D., 2015. Effective approaches to attention-based neural machine translation. arXiv preprint arXiv:1508.04025.
